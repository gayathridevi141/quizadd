
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Loginpage from './Component/Login/Loginpage';
import Staffpage from './Component/Staff/Staffpage';
import Studentpage from "./Component/Student/Studentpage";
import AddQuiz from "./Component/Staff/AddQuiz";

import StartQuiz from "./Component/Student/StartQuiz";
function App() {



  return (
    <div>

      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Loginpage />} />
          <Route path='/Staffpage' element={<Staffpage />} />
          <Route path='/Studentpage' element={<Studentpage />} />
          <Route path='/AddQuiz' element={<AddQuiz />} />
          <Route path='/EditQuiz/:index' exact element={<AddQuiz type="edit"/>} />
          <Route path='/StartQuiz' element={<StartQuiz />} />
        </Routes>
      </BrowserRouter>
    </div>

  );
}

export default App;

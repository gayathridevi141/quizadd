import React, { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { authenticated } from '../share/Auth';


import './AddQuiz.css';


const quizData =
{
    quizName: "",
    createdBy: "",
    questions: [{
        questionNo: 1,
        questionText: "",
        options: [{
            id: "",
            name: ""
        }]
    }]
}


function AddQuiz(props) {


    const { type } = props;
    const { index } = useParams();


    const [data, setData] = useState(quizData);
    // console.log(data)

    useEffect(() => {
        let quizList = JSON.parse(localStorage.getItem('Quiz'));
        const currentUser = JSON.parse(localStorage.getItem('LoggedInUser'));
        const createdBy = currentUser.name
        let copyData = { ...data };
        copyData.createdBy = createdBy;
        setData(copyData)

        //edit
        if (type === 'edit') {
            const filteredData = quizList.filter((item, i) => parseInt(index) === i)
            copyData.quizName = filteredData[0].quizName
            copyData.questions = filteredData[0].questions

            setData(copyData)
        }
    }, [])






    const LoggedInUser = authenticated();
    const navigate = useNavigate();


    useEffect(() => {
        if (LoggedInUser === undefined || LoggedInUser === null) {
            navigate('/')
        }
    })


    const handleNameChange = (e) => {
        const value = e.target.value;
        const copyData = { ...data };
        copyData.quizName = value;
        setData(copyData)

    }


    //Question Add

    const handleQuestion = (e,questionIndex) => {

        const value = e.target.value;
        const copyData = { ...data };
        const questions = copyData.questions;
        const copyIndex = questions[questionIndex];
        copyIndex.questionText = value;
        questions[questionIndex] = { ...copyIndex }
        copyData.questions = questions
        setData(copyData)

    };
//options check

    const handleOptions = (event, questionIndex, optionIndex) => {

        const value = event;
        const copyData = { ...data };
        const question = copyData.questions;
        const indexQuestion = question[questionIndex];
        const totalOption = indexQuestion.options
        // console.log(options)
        // indexOption.name=value 
        // const copyIndex = options;
        // options[Optionindex] = { ...copyIndex }
        // copyData.options = options
        // console.log(" Optionindex", copyData)
        // setData(copyData)

        const updatedCheckBox = totalOption.map((option, Index) => {
            if (optionIndex === Index) {
                option.correctAnswer = value;
            }
            else {
                option.correctAnswer = false;
            }
            return option
        })

        copyData.questions[questionIndex].options = (updatedCheckBox)

        setData(copyData);
    };



    const handleOptionChange = (e, questionIndex, optionIndex) => {

        const value = e.target.value;
        const copyData = { ...data };
        const option = copyData.questions[questionIndex].options[optionIndex]
        option.name = value;
        setData(copyData)
    };



    const handleSave = () => {

        const existingData = JSON.parse(localStorage.getItem('Quiz'));
        if (existingData !== null) {
            if (type === 'add') {
                const existingData = existingData[index];
                console.log("Data", existingData)
                existingData.push(data);
                console.log("newList", existingData)
                localStorage.setItem('Quiz', JSON.stringify(existingData));
                navigate('/Staffpage');
            } 
            else if (type === 'edit') {
                const editObj = existingData[index];
                editObj.quizName = data.quizName;
                editObj.questions = data.questions;
                localStorage.setItem('Quiz', JSON.stringify(existingData));
                navigate('/Staffpage');
            }
        }else {
            const array = []
            array.push(data);
            localStorage.setItem('Quiz', JSON.stringify(array));
            navigate('/Staffpage');
        }
    }



//question 
    const addQuestions = () => {
        const copyData = { ...data };
        const questions = copyData.questions;
        const updatedQuestions = [...questions, {
            questionNo: questions.length + 1,
            questionText: "",
            options: [{
                id: "",
                name: ""
            }]
        }]
        copyData.questions = updatedQuestions

        setData(copyData)   }

        // const addOptions = () => {
        //     setOptions(options => {
        //         return [
        //             ...options, {
        //                 id: "",
        //                 name: "",
        //                 correctAnswer: false
        //             }
        //         ];
        //     });
        // };
    
    

    const addOptions = (questionIndex) => {

        const copyData = { ...data };
        const questions = copyData.questions;
        const indexQuestion = questions[questionIndex];
        const options = indexQuestion.options;
        const updatedOptions = [...options, {
            id: options.length +1,
            name: ""

        }]

        copyData.questions[questionIndex].options = (updatedOptions)
        console.log(copyData)
        setData(copyData)
    }


    function quizHtml() {
        return <div>

            <div>
                <label id="Qname" >Quiz Name :</label>
                <input placeholder='Enter quiz name.' value={data.quizName} onChange={handleNameChange} type='text' />
            </div>


            <div>
                <button id="addbtn2" onClick={addQuestions} >Add Questions</button>
            </div>
        </div>
    }




    function quizOptions(item, questionIndex) {
        console.log("Quiz", item)
        return <div>
            <div >
                {item.options.map((option, optionIndex) => {

                    return(
                        <div key={optionIndex}><input type="radio"
                            checked={option.correctAnswer}
                            onChange={event => handleOptions(event.target.checked, questionIndex, optionIndex)}
                        />
                            <input onChange={(e) => handleOptionChange(e, optionIndex, questionIndex)} value={option.name} className="input1" type='text' /><br></br></div>
                    )
                })}
            </div>
        </div >
    }


    return (
        <div>
            <div id="head" >
                <h1 >Add Quiz</h1>
                <button id="save" onClick={handleSave}>SAVE</button></div>

            {quizHtml()}

            <div>
                <div>
                    <label id="lable">Question No</label>
                    {data.questions.map((item, questionIndex) => {
                        return (<div key={questionIndex}>
                            <div >
                               <b> {questionIndex + 1} </b> <br /> <textarea placeholder='Enter your question' onChange={(e) => handleQuestion(e, questionIndex)} id="input" value={item.questionText}></textarea>
                            </div>
                            <button id="addoption" onClick={() => addOptions(questionIndex)}>AddOptions</button>
                            {quizOptions(item, questionIndex)}
                        </div>
                        )
                    })}

                </div>
            </div>
        </div>
    )
}

export default AddQuiz

import React from "react";
import { useNavigate} from 'react-router-dom';
import './Staffpage.css';
import {Link}  from 'react-router-dom';
import {useEffect , useState} from 'react';
import {authenticated} from '../share/Auth';



function Staffpage() {
    

    const handlelogout = () => {
      localStorage.removeItem('loggedIn')
      localStorage.removeItem('loggedInUser')
      navigate('/');
    }
  
    
  const LoggedInUser = authenticated();
  const navigate = useNavigate()
  

  useEffect(() => {
    if ( LoggedInUser === undefined || LoggedInUser === null) {
      navigate('/');
    }


   },[])
   let Data = JSON.parse(localStorage.getItem('Quiz'));
   const [quizData, setQuizData] = useState(Data);
 

   const handleDelete = (quizIndex) => {
     Data.map((quiz, Index) => {
       if (Index === quizIndex) {
         const quizList = [...Data];
         quizList.splice(quizIndex, 1);
         const newList = quizList;
         setQuizData(newList);
         console.log("Data Deleted",newList)
         localStorage.setItem('Quiz', JSON.stringify(newList))
         return newList
       }
     })
   }
  //  const logout = () => {
  //   localStorage.removeItem('loggedIn')
  //   localStorage.removeItem('loggedInUser')
  //   navigate('/');
  // }

   const handleEdit = (quizIndex) => {
     navigate(`/EditQuiz/${quizIndex}`)
   }
 
 
    
  return (
    <div id="contain">
      <div id="home1">
        
          <h1 id="heading"> Welcome - {LoggedInUser.name} </h1>
        
        
          <button id="btn1" onClick={handlelogout}>Logout</button>
      </div>
      <Link to={'/AddQuiz'}>
        <button id="addbtn1">Add Questions</button>
        </Link>
      <div>
 
        {quizData.map((quiz, quizIndex) => {
          console.log(quiz)
          const quizName = quiz.quizName;
          console.log(quizName);  
          return (<div  key={quizIndex}>
            <div id="container1" >
              <div> <b> {quizName} </b>
               
                <button  className="editbtn" onClick={() => { handleEdit(quizIndex) }}>
                  EDIT
                </button>

                 <button className="deletebtn" onClick={() => { handleDelete(quizIndex) }}>
                  Delete
                </button>
              </div>
            </div>
          </div>)
        })}
      </div>
    </div >
  )
}
export default Staffpage;
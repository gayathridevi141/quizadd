import React, { useRef } from "react";
import { useNavigate } from 'react-router-dom';
import './Loginpage.css';

const records = [
  {
    id: 1,
    name: "Nisha",
    type: "Staff",
    emailid: "Nisha@gmail.com",
    password: "Nisha@123"
  },
  {
    id: 2,
    name: "Rani",
    type: "Staff",
    emailid: "Rani@gmail.com",
    password: "Rani@123"
  },
  {
    id: 3,
    name: "Nila",
    type: "Student",
    emailid: "Nila@gmail.com",
    password: "Nila@123"
  },
  {
    id: 4,
    name: "Asha",
    type: "Staff",
    emailid: "Asha@gmail.com",
    password: "Asha@123"
  },

]



function Loginpage() {

  const navigate = useNavigate();


  const email = useRef();
  const password = useRef();



  const handleSubmit = () => {
    records.filter(($user) => {

      if ((email.current.value === $user.emailid) && (password.current.value === $user.password)) {
        localStorage.setItem("loggedIn", true);
        localStorage.setItem('LoggedInUser', JSON.stringify($user));

        navigate('/Staffpage')

        alert("Login successful");
      }

      else if (($user.type === "Student") && (email.current.value === $user.emailid) && (password.current.value === $user.password)) {
        localStorage.setItem("loggedIn", true);
        localStorage.setItem('LoggedInUser', JSON.stringify($user));




        navigate('/Studentpage');
        alert("Login successful");

      }

    })
  }




  return (
    <div id="container">
      {


        <form onSubmit={handleSubmit}>
          <h1>LOGIN PAGE</h1>
          <div>
            <input type="text" placeholder="Enter email" ref={email} />
          </div>
          <div>
            <input type="password" placeholder="Enter password" ref={password} />
          </div>
          <button id="btn">Login</button>
        </form>

      }

    </div>



  );
}

export default Loginpage;
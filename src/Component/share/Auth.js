export const authenticated = () => {
    const data = JSON.parse(localStorage.getItem('LoggedInUser'))

    return data;
}

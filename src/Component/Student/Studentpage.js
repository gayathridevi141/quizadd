import React from "react";
import { useNavigate } from 'react-router-dom';
import { authenticated} from '../share/Auth';
import './Studentpage.css';
import { useEffect } from 'react';
import { Link}  from 'react-router-dom';



function Studentpage() {
   
    const LoggedInUser = authenticated();
    const navigate = useNavigate();
    
  
    useEffect(() => {
      if ( LoggedInUser === undefined || LoggedInUser === null) {
        navigate('/')
      }
    })
    
    const Navigate = useNavigate();
    const Logout=()=>{
     
        Navigate('/')
    }

    return (
 
        <>
         
        <div id = "contain">
       
            <div id="home2"><h2 id="heading"> Welcome - {LoggedInUser.name}</h2>
            
           
              
         <button  id="btn2" onClick={Logout} >Logout</button>

         
           </div>    
                  
      <Link to='/StartQuiz'>
      <button id="addbtn3">Start quiz</button>
      </Link>
                  
         <div id="container2">



         </div>
    
       
      
   
         </div>
        
        </>
     
    )
}
export default Studentpage;